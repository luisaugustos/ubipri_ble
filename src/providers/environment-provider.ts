import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";

/*
 Generated class for the EnvironmentProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */
@Injectable()
export class EnvironmentProvider {

    environment: any;

    constructor(public http: Http) {
        console.log('Hello EnvironmentProvider Provider');
    }

    consultEnvironmentByBeacon(beacon) {
        let data = {
            major: beacon.major,
            minor: beacon.minor,
            rssi: beacon.rssi
        };
        this.http.get('http://alertaitajai.com.br/ubible/app/rest/environment?major=' + data.major + '&minor=' + data.minor + '&rssi=' + data.rssi)
            .map(res => res.json()).subscribe(
            data => {
                this.environment = data
            },
            err => {
                console.log("Oops!");
            });
        return this.environment
    }


}
