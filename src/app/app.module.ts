import {ErrorHandler, NgModule} from "@angular/core";
import {IonicStorageModule} from "@ionic/storage";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {MyApp} from "./app.component";
import {BeaconsPage} from "../pages/beacons/beacons";
import {SettingsPage} from "../pages/settings/settings";
import {HomePage} from "../pages/home/home";
import {TabsPage} from "../pages/tabs/tabs";
import {BrowserModule} from "@angular/platform-browser";
import {BeaconProvider} from "../providers/beacon-provider";
import {EnvironmentProvider} from "../providers/environment-provider";
import {MapsPage} from "../pages/maps/maps";
import {GoogleMaps} from "@ionic-native/google-maps";
import {HttpModule} from "@angular/http";

@NgModule({
    declarations: [
        MyApp,
        BeaconsPage,
        SettingsPage,
        TabsPage,
        HomePage,
        MapsPage
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(MyApp),
        BrowserModule,
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        BeaconsPage,
        SettingsPage,
        TabsPage,
        HomePage,
        MapsPage
    ],
    providers: [
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Storage, BeaconProvider, GoogleMaps, EnvironmentProvider
    ]
})
export class AppModule {
}
