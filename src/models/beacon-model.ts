export class BeaconModel {

  key: string;
  uuid: string;
  major: number;
  minor: number;
  rssi: number;
  accuracy: number;

  constructor(public beacon: any) {
    this.key = beacon.key;
    this.uuid = beacon.uuid;
    this.major = beacon.major;
    this.minor = beacon.minor;
    this.rssi = beacon.rssi;
    this.accuracy = beacon.accuracy;
  }
}