export class EnvironmentModel {

  id: string;
  name: string;
  geometry: string;
  access_level: string;

  constructor(public environment: any) {
    this.id = environment.id;
    this.name = environment.name;
    this.geometry = environment.geometry;
    this.access_level = environment.access_level;
  }
}