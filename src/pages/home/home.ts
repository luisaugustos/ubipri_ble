// core stuff
import {Component, NgZone} from "@angular/core";
import {Events, NavController, Platform, AlertController} from "ionic-angular";
// plugins

// providers
import {BeaconProvider} from "../../providers/beacon-provider";
// models
import {BeaconModel} from "../../models/beacon-model";
import {EnvironmentProvider} from "../../providers/environment-provider";
import {EnvironmentModel} from "../../models/environments-model";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    beacons: BeaconModel[] = [];
    zone: any;
    environment: EnvironmentModel[] = [];

    constructor(
        public navCtrl: NavController,
        public platform: Platform,
        public beaconProvider: BeaconProvider,
        public environmentProvider: EnvironmentProvider,
        public events: Events,
        private alertCtrl: AlertController) {
        // required for UI update
        this.zone = new NgZone({enableLongStackTrace: false});
    }

    ionViewDidLoad() {
        this.platform.ready().then(() => {
            this.beaconProvider.initialise().then((isInitialised) => {
                if (isInitialised) {
                    this.listenToBeaconEvents();
                }
            });
        });
    }

    listenToBeaconEvents() {
        this.events.subscribe('didRangeBeaconsInRegion', (data) => {
            // update the UI with the beacon list
            this.zone.run(() => {
                console.log(data, 'data beacons');
                this.beacons = [];
                let beaconList = data.beacons;
                beaconList.forEach((beacon) => {
                    let beaconObject = new BeaconModel(beacon);
                    this.beacons.push(beaconObject);
                    this.environment = this.environmentProvider.consultEnvironmentByBeacon(beaconObject)
                });

            });

        });
    }

}
