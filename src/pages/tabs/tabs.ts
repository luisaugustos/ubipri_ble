import {Component} from "@angular/core";

import {HomePage} from "../home/home";
import {BeaconsPage} from "../beacons/beacons";
import {SettingsPage} from "../settings/settings";
import {MapsPage} from "../maps/maps";

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    tab0Root: any = HomePage;
    tab1Root: any = BeaconsPage;
    tab2Root: any = MapsPage;
    tab3Root: any = SettingsPage;
}
