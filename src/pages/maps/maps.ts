import {Component} from "@angular/core";
import {Events, NavController, Platform} from "ionic-angular";
import {CameraPosition, GoogleMap, GoogleMaps, GoogleMapsEvent, LatLng, Marker, MarkerOptions} from "@ionic-native/google-maps";

@Component({
    selector: 'page-maps',
    templateUrl: 'maps.html'
})

export class MapsPage {
    private map: any;

    constructor(public navCtrl: NavController, public platform: Platform, private googleMaps: GoogleMaps, public events: Events) {
    }

    ngAfterViewInit() {
        this.loadMap();
    }

    loadMap() {
        this.map = new GoogleMap('map');
        this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
            console.log('Map is ready!');
        });
    }
}
